README
======

Patches
-------
### alpha
Enables transparency

### xresources
Enables dynamic loading of settings from .Xresources or .Xdefaults

### externalpipe
https://st.suckless.org/patches/externalpipe/st-externalpipe-0.8.2.diff
Makes it possible to read the screen contents of the terminal into other
applications for editing, copying, selecting URLs...

Works well with [linkgrabber](https://st.suckless.org/patches/externalpipe/linkgrabber.sh) and [editscreen](https://st.suckless.org/patches/externalpipe/editscreen.sh).

### scrollback
https://st.suckless.org/patches/scrollback/
Enables scrolling in the terminal with Shift+PgUp/PgDown

Install patches
---------------
```bash
patch -Np1 -i xresources.diff
```
Remove patches
--------------
```bash
patch -Np1 -R -i xresources.diff
```

Peculiarities
-------------
Patches alter the file config.def.h and not config.h which is used in the build
process. The Makefile has been changed to copy config.def.h to config.h
before building whenever the former has changed.
